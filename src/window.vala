[GtkTemplate (ui = "/org/gnome/Example/window.ui")]
public class MailDemo.Window : Adw.ApplicationWindow {
    [GtkChild]
    private unowned Adw.NavigationSplitView outer_view;
    [GtkChild]
    private unowned Adw.NavigationSplitView inner_view;

    [GtkChild]
    private unowned Gtk.SearchBar inbox_search_bar;
    [GtkChild]
    private unowned Gtk.SearchEntry inbox_search_entry;
    [GtkChild]
    private unowned Gtk.SearchBar message_search_bar;
    [GtkChild]
    private unowned Gtk.SearchEntry message_search_entry;

    [GtkChild]
    private unowned Adw.Avatar avatar;
    [GtkChild]
    private unowned Gtk.ListBox folders_list;
    [GtkChild]
    private unowned Gtk.ListBox inbox_list;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        inbox_search_bar.connect_entry (inbox_search_entry);
        message_search_bar.connect_entry (message_search_entry);

        avatar.custom_image = Gdk.Texture.from_resource (@"/org/gnome/Example/avatars/angela.png");

        add_folder ("inbox-symbolic",      "Inbox",  21, true);
        add_folder ("drafts-symbolic",     "Drafts", 12);
        add_folder ("sent-symbolic",       "Sent Mail");
        add_folder ("junk-symbolic",       "Spam");
        add_folder ("user-trash-symbolic", "Trash");
        add_folder ("outbox-symbolic",     "Outbox", 3, true);
        add_folder ("archive-symbolic",    "Archive");
        add_folder_separator ();
        add_folder ("labels-symbolic",     "Labels");

        for (int i = 0; i < 10; i++) {
            add_mail (new Email () {
                avatar_image = "lea",
                participants = "Lea Król, You",
                date = "14∶22",
                topic = "Weekend Hike",
                body = "<b>You</b>: Sure, that sounds good. Let's maybe do a quick call tomorrow to figure out the details. Cheers!",
                unread = i == 0 ? 2 : 0,
            });
            add_mail (new Email () {
                participants = "noreply@bahn.de",
                date = "10∶05",
                topic = "Password Reset",
                body = "Your Deutche Bahn Password was reset. Go to bahn.de to log in and confirm your new gender.",
                unread = i == 0 ? 4 : 0,
            });
            add_mail (new Email () {
                avatar_text = "Mikael Laine",
                participants = "Mikael, Harini, John, Giorgia, Isabell",
                date = "Yesterday",
                topic = "Making Geary GNOME Mail",
                body = "Hi all, I've been working on a proposal for rebranding Geary as \"Mail\" in order to integrate better with the rest of GNOME.",
            });
            add_mail (new Email () {
                avatar_image = "graciela",
                participants = "Graciela Campos",
                date = "Feb 19",
                topic = "Meet up at FOSDEM",
                body = "I'll be in Brussels the f irst week of February so if you're there we could meet up on the 3rd of 3th of Sepuary 0th, 1971.",
            });
            add_mail (new Email () {
                avatar_image = "graciela",
                participants = "Graciela, Sander, You",
                date = "Feb 18",
                topic = "Trip to France",
                body = "I've been looking at Hotels and it's less expensive than I thought. That said, we still can't afford any of them.",
                unread = i == 0 ? 12 : 0,
            });
            add_mail (new Email () {
                avatar_image = "jaanosi",
                participants = "Jánosi Brigitta",
                attachment = true,
                date = "Feb 18",
                topic = "Cool places in Budapest",
                body = "Hey, as promised, here's my list of some of my favorite places in Budapest, though I might have missed a few.",
            });
        }

        one_pane_unapply_cb ();
        two_pane_unapply_cb ();
    }

    [GtkCallback]
    private void folders_row_activated_cb () {
        inner_view.show_content = true;
    }

    [GtkCallback]
    private void inbox_row_activated_cb () {
        outer_view.show_content = true;
    }

    [GtkCallback]
    private void one_pane_unapply_cb () {
        var row = folders_list.get_row_at_index (0);

        folders_list.selection_mode = BROWSE; // FIXME: meh
        folders_list.select_row (row);
    }

    [GtkCallback]
    private void two_pane_apply_cb () {
        if (outer_view.show_content)
            inner_view.show_content = true;
    }

    [GtkCallback]
    private void two_pane_unapply_cb () {
        var row = inbox_list.get_row_at_index (0);

        inbox_list.selection_mode = BROWSE; // FIXME: meh
        inbox_list.select_row (row);
    }

    private void add_folder_separator () {
        var row = new Gtk.ListBoxRow () {
            selectable = false,
            activatable = false,
            child = new Gtk.Separator (HORIZONTAL) {
                valign = CENTER,
            },
        };
        row.add_css_class ("separator-row");

        folders_list.append (row);
    }

    private void add_folder (string icon_name, string label, int unread = 0, bool attention = false) {
        var box = new Gtk.Box (HORIZONTAL, 0);
        box.add_css_class ("folder-row");

        box.append (new Gtk.Image.from_icon_name (icon_name));
        box.append (new Gtk.Inscription (label) {
            hexpand = true,
            xalign = 0,
            text_overflow = ELLIPSIZE_END,
        });

        if (unread > 0) {
            var counter = new Gtk.Label (@"$unread") {
                valign = CENTER,
            };

            counter.add_css_class ("counter");
            counter.add_css_class ("numeric");

            if (attention)
                counter.add_css_class ("needs-attention");

            if (unread >= 10)
                counter.add_css_class ("long");

            box.append (counter);
        }

        folders_list.append (box);
    }

    class Email {
        public string avatar_text { get; set; }
        public string avatar_image { get; set; }
        public string participants { get; set; }
        public bool attachment { get; set; }
        public string date { get; set; }
        public string topic { get; set; }
        public string body { get; set; }
        public int unread { get; set; }
    }

    private void add_mail (Email email) {
        var avatar = new Adw.Avatar (48, email.avatar_text ?? "A", email.avatar_text != null) {
            valign = START,
        };
        if (email.avatar_image != null)
            avatar.custom_image = Gdk.Texture.from_resource (@"/org/gnome/Example/avatars/$(email.avatar_image).png");

        var top_box = new Gtk.Box (HORIZONTAL, 0);

        var participants = new Gtk.Inscription (email.participants) {
            hexpand = true,
            xalign = 0,
            text_overflow = ELLIPSIZE_END,
            nat_chars = 999,
        };
        if (email.unread > 0)
            participants.add_css_class ("heading");
        top_box.append (participants);

        if (email.attachment) {
            var attachment = new Gtk.Image.from_icon_name ("mail-attachment-symbolic");
            attachment.add_css_class ("dim-label");
            top_box.append (attachment);
        }

        var date = new Gtk.Label (email.date) {
            xalign = 1,
            ellipsize = END,
        };
        date.add_css_class ("caption");
        date.add_css_class ("dim-label");
        top_box.append (date);

        var topic = new Gtk.Inscription (email.topic) {
            hexpand = true,
            xalign = 0,
            text_overflow = ELLIPSIZE_END,
        };
        topic.add_css_class (email.unread > 0 ? "caption-heading" : "caption");

        var body_box = new Gtk.Box (HORIZONTAL, 0);

        var body = new Gtk.Inscription (null) {
            hexpand = true,
            xalign = 0,
            markup = email.body,
            text_overflow = ELLIPSIZE_END,
            min_lines = 2,
        };
        body.add_css_class ("caption");
        if (email.unread == 0)
            body.add_css_class ("dim-label");
        body_box.append (body);

        if (email.unread > 0) {
            var counter = new Gtk.Label (@"$(email.unread)") {
                valign = CENTER,
            };

            counter.add_css_class ("counter");
            counter.add_css_class ("numeric");
            counter.add_css_class ("needs-attention");

            if (email.unread >= 10)
                counter.add_css_class ("long");

            body_box.append (counter);
        }

        var grid = new Gtk.Grid ();
        grid.add_css_class ("mail-row");
        grid.attach (avatar,       0, 0, 1, 3);
        grid.attach (top_box,      1, 0, 1, 1);
        grid.attach (topic,        1, 1, 1, 1);
        grid.attach (body_box,     1, 2, 1, 1);

        inbox_list.append (grid);
    }
}
